﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using Spine.Unity;
using Random = UnityEngine.Random;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class Utils
{

    public static T GetSafeComponet<T>(GameObject target) where T : MonoBehaviour
    {
        T t = target.GetComponent<T>();
        if (t == null)
            t = target.AddComponent<T>();
        return t;
    }

    public static List<int> GetRandomNoSame(int minNum, int maxNum, int ResultCount)
    {
        List<int> numbers = new List<int>();
        List<int> finals = new List<int>();
        for (int i = minNum; i < maxNum; i++)
            numbers.Add(i);

        for (int i = 0; i < ResultCount; i++)
        {
            int index = Random.Range(0, numbers.Count);
            //Debug.Log(numbers[index]);
            finals.Add(numbers[index]);
            numbers.RemoveAt(index);
        }

        return finals;
    }

    /// <summary>
    /// 一次创建多级目录中的文件
    /// </summary>
    /// <param name="path">d:/aa/bb/c.txt</param>
    public static void CreateFileRecuIfNotExist(string path)
    {
        string dir = Path.GetDirectoryName(path);
        CreateDirRecuIfNotExist(dir);

        CreateFileIfNotExist(path);
    }

    /// <summary>
    /// 一次创建多级目录
    /// </summary>
    /// <param name="dir">d:/aa/bb</param>
    public static void CreateDirRecuIfNotExist(string dir)
    {
        try
        {
            if (dir.Contains("/"))
            {
                string[] p = dir.Split('/');

                string str = p[0];
                CreateDirIfNotExist(str);

                for (int i = 1; i < p.Length; i++)
                {
                    if (!string.IsNullOrEmpty(p[i]))
                    {
                        str += "/" + p[i];
                        CreateDirIfNotExist(str);
                    }
                }
            }
        }
        catch (Exception e)
        {
            Debug.LogError(e.Message);
        }
    }

    public static void CreateDirIfNotExist(string dir)
    {
        if (!Directory.Exists(dir))
            Directory.CreateDirectory(dir);
    }

    public static FileStream CreateFileIfNotExist(string path)
    {
        if (!File.Exists(path))
            return File.Create(path);

        return null;
    }

    public static void DestroyChildObjects(Transform parent, List<string> ignore_objects)
    {
        if (parent.childCount == 0) return;

        List<GameObject> childs = new List<GameObject>();
        for (int i = 0; i < parent.childCount; i++)
        {
            Transform child = parent.GetChild(i);
            if (!ignore_objects.Contains(child.name))
                childs.Add(child.gameObject);
        }

        for (int i = 0; i < childs.Count; i++)
            GameObject.Destroy(childs[i]);
    }

    public static void GetAllSubFiles(string dir, ref List<string> files)
    {
        if (Directory.Exists(dir))
        {
            string[] fs = Directory.GetFiles(dir);
            if (fs.Length > 0)
                foreach (var item in fs)
                {
                    files.Add(item);
                }

            string[] ds = Directory.GetDirectories(dir);
            if (ds.Length > 0)
            {
                foreach (var item in ds)
                {
                    files.Add(item);
                    GetAllSubFiles(item, ref files);
                }
            }
        }
        else
        {
            Debug.LogError("不存在这个目录");
        }
    }

    public static void CopyDir(string srcPath, string aimPath)
    {
        try
        {
            // 检查目标目录是否以目录分割字符结束如果不是则添加之
            if (aimPath[aimPath.Length - 1] != Path.DirectorySeparatorChar)
                aimPath += Path.DirectorySeparatorChar;
            // 判断目标目录是否存在如果不存在则新建之
            if (!Directory.Exists(aimPath))
                Directory.CreateDirectory(aimPath);
            // 得到源目录的文件列表，该里面是包含文件以及目录路径的一个数组
            // 如果你指向copy目标文件下面的文件而不包含目录请使用下面的方法
            // string[] fileList = Directory.GetFiles(srcPath);
            string[] fileList = Directory.GetFileSystemEntries(srcPath);
            // 遍历所有的文件和目录
            foreach (string file in fileList)
            {
                // 先当作目录处理如果存在这个目录就递归Copy该目录下面的文件
                if (Directory.Exists(file))
                    CopyDir(file, aimPath + Path.GetFileName(file));
                // 否则直接Copy文件
                else
                    File.Copy(file, aimPath + Path.GetFileName(file), true);
            }
        }
        catch (Exception e)
        {
            Debug.LogError("无法复制" + e.Message);
        }
    }

    public static bool IsEnterView(Vector3 screenPos)
    {
        Vector3 viewpos = Camera.main.ScreenToViewportPoint(screenPos);
        if (viewpos.x > -0.1f && viewpos.y > -0.1f && viewpos.x <= 1.1f && viewpos.y <= 1.1f)
        {
            return true;
        }
        return false;
    }

    public static bool IsOnUI(GraphicRaycaster caster, Vector3 pos)
    {
        PointerEventData pData = new PointerEventData(EventSystem.current);

        pData.position = pos;
        pData.delta = Vector2.zero;
        pData.scrollDelta = Vector2.zero;

        List<UnityEngine.EventSystems.RaycastResult> canvasHits = new List<UnityEngine.EventSystems.RaycastResult>();
        caster.Raycast(pData, canvasHits);


        return canvasHits.Count > 0;
    }
}